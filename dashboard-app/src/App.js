import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";

import moment from "moment";
import numeral from "numeral";
import cubejs from "@cubejs-client/core";
import Chart from "./Chart.js";


const API_URL = process.env.NODE_ENV === 'production' ? '' : 'http://localhost:4000'
const cubejsApi = cubejs(process.env.REACT_APP_CUBEJS_TOKEN, {
  apiUrl: `${API_URL}/cubejs-api/v1`
})

const numberFormatter = item => numeral(item).format("0,0");
const dateFormatter = item => moment(item).format("MMM YY");

const renderSingleValue = (resultSet, key) => (
  <h1 height={300}>{numberFormatter(resultSet.chartPivot()[0][key])}</h1>
);

class App extends Component {
  render() {
    return (
      <Container fluid>
        <Row>
          <Col sm="4">
          <Chart
              cubejsApi={cubejsApi}
              title="Total Orders"
              query={{ measures: ["Orders.count"] }}
              render={resultSet => renderSingleValue(resultSet, "Orders.count")}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;