cube(`FarrowToFinish`, {
  sql: `SELECT * FROM porklogic.farrow_to_finish`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [date]
    }
  },
  
  dimensions: {
    pig: {
      sql: `pig`,
      type: `string`
    },
    
    farm: {
      sql: `farm`,
      type: `string`
    },
    
    barn: {
      sql: `barn`,
      type: `string`
    },
    
    pen: {
      sql: `pen`,
      type: `string`
    },
    
    category: {
      sql: `category`,
      type: `string`
    },
    
    age: {
      sql: `age`,
      type: `string`
    },
    
    ageGroup: {
      sql: `age_group`,
      type: `string`
    },
    
    wellnessScoreGroup: {
      sql: `wellness_score_group`,
      type: `string`
    },
    
    date: {
      sql: `date`,
      type: `time`
    }
  }
});
